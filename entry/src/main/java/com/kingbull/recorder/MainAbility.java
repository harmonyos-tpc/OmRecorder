/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kingbull.recorder;

import com.kingbull.recorder.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;

public class MainAbility extends Ability {
    private int mPermissionReqCode = 100;
    private String[] mNeedPermissions = {"ohos.permission.MICROPHONE","ohos.permission.READ_USER_STORAGE","ohos.permission.WRITE_USER_STORAGE"};
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        if (verifyCallingPermission(mNeedPermissions[0]) != IBundleManager.PERMISSION_GRANTED) {
            requestPermissionsFromUser(mNeedPermissions, mPermissionReqCode);
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        if (requestCode == mPermissionReqCode) {
            if (grantResults[0] != IBundleManager.PERMISSION_GRANTED) {
                terminateAbility();
            }
        } else {
            terminateAbility();
        }
    }

}
