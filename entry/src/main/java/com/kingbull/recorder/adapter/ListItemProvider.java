/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kingbull.recorder.adapter;

import com.kingbull.recorder.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * ListItemProvider extends BaseItemProvider
 *
 * @since 2020-07-22
 */
public class ListItemProvider extends BaseItemProvider {
    private static final String TAG = "ListItemProvider" ;

    private Context context;

    private String[] itemList;

    public ListItemProvider(Context context, String[] itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.length;
    }

    @Override
    public Object getItem(int position) {
        return itemList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        return getRootView(position);
    }

    private Component getRootView(int position) {
        Component rootView = LayoutScatter.getInstance(context)
            .parse(ResourceTable.Layout_list_item, null, false);
        Text deviceName = (Text) rootView.findComponentById(ResourceTable.Id_item_name);

        String bluetoothDevice = itemList[position];
        deviceName.setText(bluetoothDevice);
        return rootView;
    }

}