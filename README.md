## OmRecorder

## Introduction
  A Simple Pcm / Wav audio recorder with nice api.

  Record Pcm audio
  Record Wav audio
  Configure audio source to have desired output
  Record with pause / resume feature

## Usage Instruction

Add these permissions into your config.xml
    ohos.permission.WRITE_USER_STORAGE
    ohos.permission.MICROPHONE


 recorder = OmRecorder.wav(
                new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                    @Override
                    public void onAudioChunkPulled(AudioChunk audioChunk) {
                        animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                    }
                }), file());

   recorder = OmRecorder.pcm(
                  new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                      @Override public void onAudioChunkPulled(AudioChunk audioChunk) {
                          animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                      }
                  }), file());

## For Skip Silence

  // FOR SKIP SILENCE
 recorder = OmRecorder.wav(
        new PullTransport.Noise(mic(),
            new PullTransport.OnAudioChunkPulledListener() {
              @Override public void onAudioChunkPulled(AudioChunk audioChunk) {
                animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
              }
            },
            new WriteAction.Default(),
            new Recorder.OnSilenceListener() {
              @Override public void onSilence(long silenceTime) {
                Log.e("silenceTime", String.valueOf(silenceTime));
                Toast.makeText(WavRecorderActivity.this, "silence of " + silenceTime + " detected",
                    Toast.LENGTH_SHORT).show();
              }
            }, 200
        ), file()
    );

 @NonNull private File file() {
    return new File(Environment.getExternalStorageDirectory(), "demo.wav");
  }

## Configure Audio Source

  return new PullableSource.Default(
         new AudioRecordConfig.Default(
             MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
             AudioFormat.CHANNEL_IN_MONO, 44100
         )
     );

## Start & Stop Recording

       recorder.startRecording();
       recorder.stopRecording();

## Pause & Resume Recording

       recorder.pauseRecording();
       recorder.resumeRecording();

Note: PCM recording is for 16-bit and should play with PCM player

## Installation instructions:
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])

Method 2:

Add following dependencies in entry build.gradle:**

    entry build.gradle:
    dependencies {
            implementation project(':om_recorder')
    }

## LICENSE :

Copyright 2017 Kailash Dabhi (Kingbull Technology)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


